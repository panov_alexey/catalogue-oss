// @flow

import webpack from 'webpack'
import path from 'path'

import { WDS_PORT } from './src/shared/config'
import { isProd } from './src/shared/util'


export default {
  entry: [
    'react-hot-loader/patch',
    './src/client',
  ],
  output: {
    filename: 'js/bundle.js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: isProd ? '/static/' : `http://localhost:${WDS_PORT}/dist/`,
  },
  module: {
    rules: [
      { test: /\.(js|jsx)$/, use: 'babel-loader', exclude: /node_modules/ },
      {
        test: /\.(png|jpg|gif)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: isProd ? '/static/' : `http://localhost:${WDS_PORT}/dist/_[name].[ext]`,
            },
          },
        ],
      },
    ],
  },
  devtool: isProd ? false : 'source-map',
  resolve: {
    alias: {
      $container: path.resolve(__dirname, './src/shared/container'),
      $component: path.resolve(__dirname, './src/shared/component'),
    },
    extensions: ['.js', '.jsx'],
  },
  devServer: {
    port: WDS_PORT,
    hot: true,
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
  },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
  ],
}
