// @flow

import 'isomorphic-fetch'

import { STATIC_PATH } from '../shared/config'


/*
 * retrieving items for catalogue. Maybe here should be some caching?
 */
export const catalogueItems = (req: any): Promise<*> => {
  const baseUrl = req ? `${req.protocol}://${req.get('Host')}` : ''

  return fetch(`${baseUrl}${STATIC_PATH}/data/catalogue.json`, { method: 'GET' })
    .then(response => response.json())
    .then(data => ({
      catalogue: data.items,
    }))
}

// export const homePage = (req: any): Promise<*> => catalogueItems(req)
export const homePage = () => null
export const addItemPage = () => null
