// @flow

import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'

import mainReducer from '../shared/reducer'
import AddItemAction from '../shared/action/AddItemAction'


const initStore = (plainPartialState: ?Object) => {
  let preloadedState

  if (plainPartialState && plainPartialState.catalogue) {
    plainPartialState.catalogue.forEach((itemData) => {
      preloadedState = mainReducer(
        preloadedState,
        new AddItemAction(itemData),
      )
    })
  }

  return createStore(mainReducer, preloadedState, applyMiddleware(thunkMiddleware))
}

export default initStore
