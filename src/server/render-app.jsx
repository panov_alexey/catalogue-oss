// @flow

import React from 'react'
import ReactDOMServer from 'react-dom/server'
import Helmet from 'react-helmet'
import { Provider } from 'react-redux'
import { StaticRouter } from 'react-router'
import { SheetsRegistry, JssProvider } from 'react-jss'

import getMuiTheme from 'material-ui/styles/getMuiTheme'

import initStore from './init-store'
import App from '../shared/App'
import { APP_CONTAINER_CLASS, JSS_SSR_CLASS, STATIC_PATH, WDS_PORT } from '../shared/config'
import { isProd } from '../shared/util'


const renderApp = (req: any, plainPartialState: ?Object, routerContext: ?Object = {}) => {
  const store = initStore(plainPartialState)
  const sheets = new SheetsRegistry()
  const muiTheme = getMuiTheme(
    null,
    { userAgent: req.headers['user-agent'] },
  )
  const wrap = (
    <Provider store={store}>
      <StaticRouter location={req.url} context={routerContext}>
        <JssProvider registry={sheets}>
          <App muiTheme={muiTheme} />
        </JssProvider>
      </StaticRouter>
    </Provider>
  )
  const appHtml = ReactDOMServer.renderToString(wrap)
  const head = Helmet.rewind()

  return (
    `<!doctype html>
    <html>
      <head>
        ${head.title}
        ${head.meta}
        <link rel="stylesheet" href="${isProd ? STATIC_PATH : `http://localhost:${WDS_PORT}/public`}/css/reset.css">
        <style class="${JSS_SSR_CLASS}">${sheets.toString()}</style>
      </head>
      <body>
        <div class="${APP_CONTAINER_CLASS}">${appHtml}</div>
        <script>
          window.__PRELOADED_STATE__ = ${JSON.stringify(store.getState())}
        </script>
        <script src="${isProd ? STATIC_PATH : `http://localhost:${WDS_PORT}/dist`}/js/bundle.js"></script>
      </body>
    </html>`
  )
}

export default renderApp
