// @flow

import {
  homePage,
  addItemPage,
  catalogueItems,
} from './controller'

import {
  HOME_PAGE_ROUTE,
  ADD_ITEM_PAGE_ROUTE,
  CATALOGUE_ITEMS_ROUTE,
} from '../shared/routes'

import renderApp from './render-app'


export default (app: Object) => {
  app.get(HOME_PAGE_ROUTE, (req, res) => {
    res.send(renderApp(req, homePage()))
    // homePage(req)
    //   .then(data => res.send(renderApp(req, data)))
  })

  app.get(ADD_ITEM_PAGE_ROUTE, (req, res) => {
    res.send(renderApp(req, addItemPage()))
  })

  app.get(CATALOGUE_ITEMS_ROUTE, (req, res) => {
    catalogueItems(req)
      .then(data => res.json(data))
  })

  app.get('/500', () => {
    throw Error('Fake Internal Server Error')
  })

  app.get('*', (req, res) => {
    res.status(404).send(renderApp(req))
  })

  // eslint-disable-next-line no-unused-vars
  app.use((err, req, res, next) => {
    // eslint-disable-next-line no-console
    console.error(err.stack)
    res.status(500).send('Something went wrong!')
  })
}
