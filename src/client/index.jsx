// @flow

import 'babel-polyfill'

import React from 'react'
import ReactDOM from 'react-dom'
import { AppContainer } from 'react-hot-loader'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware, compose } from 'redux'
import { BrowserRouter } from 'react-router-dom'
import thunkMiddleware from 'redux-thunk'

import App from '../shared/App'
import mainReducer from '../shared/reducer'
import { APP_CONTAINER_SELECTOR, JSS_SSR_SELECTOR } from '../shared/config'
import { isProd } from '../shared/util'
import * as ls from './localStorage'


/* eslint-disable */
const composeEnhancers = (isProd ? null : window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose
const preloadedState = window.__PRELOADED_STATE__
/* eslint-enable */

const store = createStore(
  mainReducer,
  preloadedState,
  composeEnhancers(applyMiddleware(thunkMiddleware)),
)

const root = document.querySelector(APP_CONTAINER_SELECTOR)
const wrap = (AppComponent, reduxStore) => (
  <Provider store={reduxStore}>
    <BrowserRouter>
      <AppContainer>
        <AppComponent />
      </AppContainer>
    </BrowserRouter>
  </Provider>
)


if (root) {
  store.subscribe(() => {
    const { catalogue: { items } } = store.getState()
    ls.setCatalogue(items)
  })

  // flow-disable-next-line
  ReactDOM.hydrate(wrap(App, store), root)

  if (module.hot) {
    // flow-disable-next-line
    module.hot.accept('../shared/App', () => {
      // eslint-disable-next-line global-require
      const NextApp = require('../shared/App').default
      // flow-disable-next-line
      ReactDOM.hydrate(wrap(NextApp, store), root)
    })
  }

  const jssServerSide = document.querySelector(JSS_SSR_SELECTOR)
  if (jssServerSide && jssServerSide.parentNode) {
    jssServerSide.parentNode.removeChild(jssServerSide)
  }
} else {
  throw new Error('no root element found')
}
