// @flow

import type { ItemData } from '../shared/action/AddItemAction'


const CATALOGUE: string = 'catalogue'

export const getCatalogue = () => {
  let catalogue: ?Array<ItemData>

  if (typeof window !== 'undefined') {
    // window.localStorage.setItem(CATALOGUE, null) // - uncomment to reset
    catalogue = JSON.parse(window.localStorage.getItem(CATALOGUE))
  }
  return catalogue
}

export const setCatalogue = (catalogue: Array<ItemData>) => {
  if (typeof window !== 'undefined') {
    window.localStorage.setItem(CATALOGUE, JSON.stringify(catalogue))
  }
}

