// @flow

import * as React from 'react'
import Helmet from 'react-helmet'
import injectSheet from 'react-jss'

import Header from '../header'
import AddItemForm from '../../container/addItemForm'
import styles from './styles'
import { APP_NAME } from '../../config'


type Props = {
  classes: Object
}

class HomePage extends React.PureComponent<Props> {
  render() {
    const { classes } = this.props

    return (
      <div className={classes.wrap}>
        <Helmet
          meta={[
            { name: 'description', content: 'Hello App is an app to say hello' },
            { property: 'og:title', content: APP_NAME },
          ]}
        />
        <Header />
        <AddItemForm />
      </div>
    )
  }
}

export default injectSheet(styles)(HomePage)
