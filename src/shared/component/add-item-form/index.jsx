// @flow

import * as React from 'react'
import injectStyles from 'react-jss'
import { withRouter } from 'react-router-dom'

import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'

import DialogPopUp from '../dialog-pop-up'
import styles from './styles'
import { ADMIN_USER_ROLE } from '../../config'
import {
  HOME_PAGE_ROUTE,
} from '../../routes'


type Props = {
  classes: any,
  history: any,
  name: string,
  description: string,
  price: number,
  userRole: string,
  updateFormData: Function,
  addItem: Function,
};

type State = {
  name: {
    errorMsg: string,
  },
  description: {
    errorMsg: string,
  },
  price: {
    errorMsg: string,
  },
  dialog: boolean,
}

class AddItemForm extends React.PureComponent<Props, State> {
  static NAME_FIELD_ID: string = 'name'
  static PRICE_FIELD_ID: string = 'price'
  static DESCRIPTION_FIELD_ID: string = 'description'

  static FIELD_IS_REQUIRED_ERROR_MSG: string = 'обязательное поле'
  static TOO_SHORT_ERROR_MSG: string = 'не меньше 8 символов'
  static INVALID_VALUE_ERROR_MSG: string = 'невалидное значение'

  state: State = {
    name: {
      errorMsg: '',
    },
    description: {
      errorMsg: '',
    },
    price: {
      errorMsg: '',
    },
    dialog: false,
  }

  /**
   *
   */
  validate = (id: string): string => {
    const { name, description, price } = this.props
    let value

    switch (id) {
      case AddItemForm.NAME_FIELD_ID:
        value = name.trim()
        if (value.length === 0) {
          return AddItemForm.FIELD_IS_REQUIRED_ERROR_MSG
        } else if (value.length < 8) {
          return AddItemForm.TOO_SHORT_ERROR_MSG
        }
        break

      case AddItemForm.DESCRIPTION_FIELD_ID:
        value = description.trim()
        if (value.length === 0) {
          return AddItemForm.FIELD_IS_REQUIRED_ERROR_MSG
        }
        break

      case AddItemForm.PRICE_FIELD_ID:
        value = price
        if (value.length === 0) {
          return AddItemForm.FIELD_IS_REQUIRED_ERROR_MSG
        } else if (parseFloat(value) === 0) {
          return AddItemForm.INVALID_VALUE_ERROR_MSG
        }
        break

      default:
        return ''
    }

    return ''
  }

  /**
   *
   */
  fieldListener = (e) => {
    const { target } = e
    const { updateFormData } = this.props

    updateFormData(target.value, target.id)
  }

  /**
   *
   */
  addBtnListener = () => {
    const nameMsg = this.validate(AddItemForm.NAME_FIELD_ID)
    const descriptionMsg = this.validate(AddItemForm.DESCRIPTION_FIELD_ID)
    const priceMsg = this.validate(AddItemForm.PRICE_FIELD_ID)
    const {
      name, description, price, addItem,
    } = this.props

    this.setState({
      name: { errorMsg: nameMsg },
      description: { errorMsg: descriptionMsg },
      price: { errorMsg: priceMsg },
    })

    if (!nameMsg && !descriptionMsg && !priceMsg) {
      addItem(name, description, price)
      this.setState({
        dialog: true,
      })
    }
  }

  /**
   *
   */
  dialogContinueBtnListener = () => {
    this.setState({
      dialog: false,
    })
  }

  /**
   *
   */
  dialogGotoBtnListener = () => {
    const { history } = this.props

    history.push(HOME_PAGE_ROUTE)
  }

  render() {
    const {
      name, description, price, userRole, classes: css,
    } = this.props

    return (
      <div className={css.wrap}>
        {
          userRole === ADMIN_USER_ROLE ? (
            <form action="#">
              <TextField
                id={AddItemForm.NAME_FIELD_ID}
                value={name}
                fullWidth
                hintText="Введите название"
                floatingLabelText="Название"
                errorText={this.state.name.errorMsg}
                onChange={this.fieldListener}
              /><br />
              <TextField
                id={AddItemForm.PRICE_FIELD_ID}
                value={price ? price : ''} // eslint-disable-line no-unneeded-ternary
                fullWidth
                hintText="Введите цену"
                floatingLabelText="Цена"
                errorText={this.state.price.errorMsg}
                onChange={this.fieldListener}
              /><br />
              <TextField
                id={AddItemForm.DESCRIPTION_FIELD_ID}
                value={description}
                className={css.textArea}
                fullWidth
                multiLine
                hintText="Введите описание"
                floatingLabelText="описание"
                rows={4}
                errorText={this.state.description.errorMsg}
                onChange={this.fieldListener}
              /><br />
              <RaisedButton
                label="Добавить"
                primary
                onClick={this.addBtnListener}
              />
            </form>
          ) : (
            <p className={css.userRoleWarn}>Для управления необходимо иметь права администратора</p>
          )
        }
        {
          this.state.dialog && (
            <DialogPopUp
              title="Информация"
              message="Товар успешно добавлен"
              firstActionLabel="Продолжить"
              secondActionLabel="Перейти в каталог"
              firstActionBtnListener={this.dialogContinueBtnListener}
              secondActionBtnListener={this.dialogGotoBtnListener}
            />
          )
        }
      </div>
    )
  }
}

export default withRouter(injectStyles(styles)(AddItemForm))
