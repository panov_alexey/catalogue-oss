// @flow

export default {
  wrap: {
    position: 'relative',
    backgroundColor: '#eee',
    borderRadius: '2px',
    padding: '20px',
  },
  textArea: {
    marginBottom: '20px',
  },
  userRoleWarn: {
    textAlign: 'center',
    color: '#888',
  },
}
