// @flow
// home page styles:
import { STATIC_PATH } from '../../shared/config'

export const fontsPath: string = `${STATIC_PATH}/fonts/`;
export const mainFont: string = 'Roboto-Regular';
export const mainFontFamily: string = 'roboto-regular';
export const secFont: string = 'Cervo-Regular';
export const secFontFamily: string = 'cervo-regular';

const fontsSetup = [
  { family: mainFontFamily, name: mainFont },
  { family: secFontFamily, name: secFont },
]

export default {
  '@font-face': fontsSetup.map(({ family, name }) => ({
    fontFamily: family,
    src: `
      url('${fontsPath}${name}.eot'),
      url('${fontsPath}${name}.eot?#iefix') format('embedded-opentype'),
      url('${fontsPath}${name}.woff2') format('woff2'),
      url('${fontsPath}${name}.woff') format('woff'),
      url('${fontsPath}${name}.ttf') format('truetype')`,
    fontWeight: 'normal',
    fontStyle: 'normal',
  })),
  pageWrap: {
    width: '1200px',
    margin: '0 auto',
    paddingTop: '10px',
    minHeight: '100vh',
    fontFamily: mainFontFamily,
    color: 'white',
  },
}
