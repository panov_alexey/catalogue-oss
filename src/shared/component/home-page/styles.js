// @flow
// home page styles:
import styles from '../styles'


export default {
  '@font-face': styles['@font-face'],
  wrap: {
    extend: styles.pageWrap,
  },
}
