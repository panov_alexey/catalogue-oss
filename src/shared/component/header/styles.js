export default {
  wrap: {
    display: 'flex',
    position: 'relative',
    borderRadius: '2px',
    backgroundColor: '#eee',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '20px',
    color: '#121212',
    marginBottom: '2px',
  },
  logo: {
    display: 'block',
    height: '40px',
  },
}
