// @flow

import React from 'react'
import injectSheet from 'react-jss'
import { Link } from 'react-router-dom'

import IconMenu from 'material-ui/IconMenu'
import MenuItem from 'material-ui/MenuItem'
import IconButton from 'material-ui/IconButton'
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert'

import logo from './logo.png'
import InfoBox from '../../container/infoBox'
import styles from './styles'
import {
  HOME_PAGE_ROUTE,
  ADD_ITEM_PAGE_ROUTE,
} from '../../routes'


type Props = {
  classes: Object
}

class Header extends React.PureComponent<Props> {
  render() {
    const { classes: css } = this.props

    return (
      <div className={css.wrap}>
        <Link to={HOME_PAGE_ROUTE} href={HOME_PAGE_ROUTE}>
          <img className={css.logo} src={logo} alt="logo" />
        </Link>
        <InfoBox />
        <IconMenu
          className={css.menuBtn}
          iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
          anchorOrigin={{ horizontal: 'left', vertical: 'top' }}
          targetOrigin={{ horizontal: 'left', vertical: 'top' }}
        >
          <MenuItem
            containerElement={<Link to={HOME_PAGE_ROUTE} href={HOME_PAGE_ROUTE} />}
            primaryText="Каталог"
          />
          <MenuItem
            containerElement={<Link to={ADD_ITEM_PAGE_ROUTE} href={ADD_ITEM_PAGE_ROUTE} />}
            primaryText="Добавить товар"
          />
        </IconMenu>
      </div>
    )
  }
}

export default injectSheet(styles)(Header)
