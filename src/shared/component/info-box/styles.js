import { secFontFamily } from '../styles'

export default {
  wrap: {
    display: 'flex',
    position: 'absolute',
    right: '100px',
    top: '50%',
    transform: 'translateY(-50%)',
  },
  column: {
    width: '120px',
    padding: '0 20px',
    textAlign: 'center',
  },
  columnTitle: {
    fontSize: '10px',
    color: '#888',
    marginBottom: '6px',
  },
  columnText: {
    fontFamily: secFontFamily,
    fontSize: '20px',
    color: '#121212',
  },
  removeBtn: {
    marginLeft: '20px',
  },
}
