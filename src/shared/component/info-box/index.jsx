// @flow

import React from 'react'
import injectSheet from 'react-jss'

import RaisedButton from 'material-ui/RaisedButton'

import DialogPopUp from '../dialog-pop-up'
import styles from './styles'
import { formatNumber } from '../../util'
import { ADMIN_USER_ROLE } from '../../config'
import type { Item } from '../../reducer/items'


type Props = {
  classes: Object,
  items: Array<Item>,
  userRole: string,
  retrieveItems: Function,
  removeAll: Function,
}

type State = {
  isPendingRemove: boolean
}

class InfoBox extends React.PureComponent<Props, State> {
  state: State = {
    isPendingRemove: false,
  }

  /**
   *
   */
  componentDidMount = () => {
    const { retrieveItems } = this.props
    retrieveItems()
  }

  /**
   *
   */
  removeBtnClickListener = () => {
    this.setState({
      isPendingRemove: true,
    })
  }

  /**
   *
   */
  dialogCancelBtnListener = () => {
    this.setState({
      isPendingRemove: false,
    })
  }

  /**
   *
   */
  dialogRemoveBtnListener = () => {
    const { removeAll } = this.props

    removeAll()
    this.setState({
      isPendingRemove: false,
    })
  }

  /**
   *
   */
  render() {
    const { classes: css, items, userRole } = this.props
    const { isPendingRemove } = this.state
    const amount = items.length
    const totalPrice = items.reduce((sum, current) => (sum + current.price), 0)
    const averagePrice = amount !== 0 ? totalPrice / amount : 0

    return (
      <div className={css.wrap}>
        <div className={css.column}>
          <p className={css.columnTitle}>Кол-во</p>
          <p className={css.columnText}>{amount}</p>
        </div>
        <div className={css.column}>
          <p className={css.columnTitle}>Общая цена</p>
          <p className={css.columnText}>{formatNumber(totalPrice, 2)}</p>
        </div>
        <div className={css.column}>
          <p className={css.columnTitle}>Средняя цена</p>
          <p className={css.columnText}>{formatNumber(averagePrice, 2)}</p>
        </div>
        {
          userRole === ADMIN_USER_ROLE && (
            <div className={css.removeBtn}>
              <RaisedButton
                label="удалить все"
                disabled={!items.length}
                onClick={this.removeBtnClickListener}
              />
            </div>
          )
        }
        {
          isPendingRemove && (
            <DialogPopUp
              title="Подтверждение"
              message={
                (
                  <p>Удалить все?</p>
                )
              }
              firstActionLabel="Отмена"
              secondActionLabel="Удалить"
              firstActionBtnListener={this.dialogCancelBtnListener}
              secondActionBtnListener={this.dialogRemoveBtnListener}
            />
          )
        }
      </div>
    )
  }
}

export default injectSheet(styles)(InfoBox)
