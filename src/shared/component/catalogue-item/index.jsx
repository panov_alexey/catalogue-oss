// @flow

import * as React from 'react'
import injectSheet from 'react-jss'

import RaisedButton from 'material-ui/RaisedButton'

import styles from './styles'
import { STATIC_PATH, ADMIN_USER_ROLE } from '../../../shared/config'
import { formatNumber } from '../../util'
import type { Item } from '../../reducer/items'


type Props = {
  classes: any,
  item: Item,
  userRole: string,
  remove: Function,
}

class CatalogueItem extends React.PureComponent<Props> {
  /**
   *
   */
  removeBtnClickListener = () => {
    const { remove, item } = this.props

    remove(item.id)
  }

  /**
   *
   */
  render() {
    const {
      classes: css,
      item: {
        title, description, picURL, price,
      },
      userRole,
    } = this.props

    return (
      <div className={css.wrap}>
        <div className={css.pic} style={{ backgroundImage: `url(${STATIC_PATH}/images/${picURL})` }} />
        <div className={css.texts}>
          <p className={css.title}>{title}</p>
          <p className={css.description}>{description}</p>
        </div>
        <p className={css.price}>{formatNumber(price, 0)}</p>
        {
          userRole === ADMIN_USER_ROLE && <RaisedButton
            className={css.removeBtn}
            label="удалить"
            primary
            onClick={this.removeBtnClickListener}
          />
        }
      </div>
    )
  }
}

export default injectSheet(styles)(CatalogueItem)
