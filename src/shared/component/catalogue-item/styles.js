import { secFontFamily } from '../styles'

export default {
  wrap: {
    position: 'relative',
    flex: '0 0 calc(25% - 1px)',
    margin: '0 1px 1px 0',
    color: '#888',
    backgroundColor: '#eee',
    '&:nth-child(4n)': {
      flex: '0 0 calc(25%)',
      marginRight: '0',
    },
  },
  pic: {
    display: 'block',
    width: '100%',
    maxWidth: '100%',
    height: '200px',
    marginBottom: '20px',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  title: {
    fontFamily: secFontFamily,
    fontSize: '20px',
    color: '#555',
    padding: '0 10px',
    marginBottom: '10px',
  },
  description: {
    fontSize: '14px',
    padding: '0 10px',
    marginBottom: '75px',
  },
  price: {
    position: 'absolute',
    bottom: 5,
    left: 0,
    fontFamily: secFontFamily,
    fontSize: '30px',
    color: 'orangered',
    padding: '0 10px',
  },
  removeBtn: {
    position: 'absolute',
    bottom: 10,
    right: 10,
  },
}
