// @flow

export default {
  wrap: {
    position: 'relative',
  },
  actions: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
}
