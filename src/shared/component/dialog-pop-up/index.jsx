// @flow

import * as React from 'react'
import injectSheet from 'react-jss'

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

import styles from './styles'


type Props = {
  classes: any,
  title: string,
  message: any,
  firstActionLabel: string,
  secondActionLabel: string,
  firstActionBtnListener: ?Function,
  secondActionBtnListener: ?Function,
};


class DialogPopUp extends React.PureComponent<Props> {
  render() {
    const {
      classes: css,
      title,
      message,
      firstActionLabel,
      firstActionBtnListener,
      secondActionLabel,
      secondActionBtnListener,
    } = this.props

    return (
      <Dialog
        title={title}
        modal
        open
        actionsContainerClassName={css.actions}
        actions={[
          <FlatButton
            primary
            label={firstActionLabel}
            onClick={firstActionBtnListener}
          />,
          <FlatButton
            disabled={false}
            primary
            label={secondActionLabel}
            onClick={secondActionBtnListener}
          />,
        ]}
      >
        {message}
      </Dialog>
    )
  }
}

export default injectSheet(styles)(DialogPopUp)
