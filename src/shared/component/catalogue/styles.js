export default {
  wrap: {
    position: 'relative',
    display: 'flex',
    flexWrap: 'wrap',
    borderRadius: '2px',
    overflow: 'hidden',
  },
  preloader: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(18, 18, 18, 0.5)',
  },
  strong: {
    color: 'black',
  },
  progress: {
    position: 'fixed',
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%) scale(0.15)',
    // animation: 'rotation 3s infinite linear',
  },
  '@keyframes rotation': {
    '0%': {
      transform: 'translate(-50%, -50%) rotate(0deg)',
    },
    '100%': {
      transform: 'translate(-50%, -50%) rotate(360deg)',
    },
  },
}
