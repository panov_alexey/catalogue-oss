// @flow

import * as React from 'react'
import injectSheet from 'react-jss'

import CatalogueItem from '../../container/catalogueItem'
import DialogPopUp from '../dialog-pop-up'
import styles from './styles'
import { STATIC_PATH } from '../../config'
import type { Item } from '../../reducer/items'


type Props = {
  classes: any,
  items: Array<Item>,
  isPending: boolean,
  retrieveItems: Function,
  removeItem: Function,
}

type State = {
  pendingRemoveID: ?string,
}

class Catalogue extends React.Component<Props, State> {
  static defaultProps: Props = {
    classes: {},
    items: [],
    isPending: false,
    retrieveItems: () => { throw Error('retrieveItems must be defined') },
    removeItem: () => { throw Error('removeItem must be defined') },
  }

  state: State = {
    pendingRemoveID: null,
  }

  /**
   *
   */
  componentDidMount = () => {
    const { retrieveItems } = this.props

    retrieveItems()
  }

  /**
   *
   */
  getItemByID = (id: ?string): ?Item => {
    if (!id) {
      return null
    }

    const { items } = this.props
    const targetItem = items.filter(item => item.id === id)[0]

    return targetItem
  }

  /**
   *
   */
  dialogRemoveBtnListener = () => {
    const { removeItem } = this.props
    const { pendingRemoveID } = this.state

    removeItem(pendingRemoveID)
  }

  /**
   *
   */
  dialogCancelBtnListener = () => {
    this.setState({
      pendingRemoveID: null,
    })
  }

  /**
   *
   */
  removeItemListener = (id: string) => {
    this.setState({
      pendingRemoveID: id,
    })
  }

  /**
   *
   */
  render() {
    const { classes: css, items, isPending } = this.props
    const { pendingRemoveID } = this.state
    const removingItem = this.getItemByID(pendingRemoveID)

    return (
      <div className={css.wrap}>
        {
          items.map(item => (
            <CatalogueItem key={item.id} item={item} remove={this.removeItemListener} />
          ))
        }
        {
          isPending && (
            <div className={css.preloader}>
              <div className={css.progress}>
                <img src={`${STATIC_PATH}/images/progress.webp`} alt="progress" />
              </div>
            </div>
          )
        }
        {
          removingItem && (
            <DialogPopUp
              title="Подтверждение"
              message={
                (
                  <p>Удалить <strong className={css.strong}>{removingItem.title}?</strong></p>
                )
              }
              firstActionLabel="Отмена"
              secondActionLabel="Удалить"
              firstActionBtnListener={this.dialogCancelBtnListener}
              secondActionBtnListener={this.dialogRemoveBtnListener}
            />
          )
        }
      </div>
    )
  }
}

export default injectSheet(styles)(Catalogue)
