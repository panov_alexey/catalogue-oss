// @flow

import { connect } from 'react-redux'

import InfoBox from '../component/info-box'
import RemoveAllItemsAction from '../action/RemoveAllItemsAction'
import { retrieveItemsAction } from '../action/RetrieveItemsAction'

const mapStateToProps = state => ({
  items: state.catalogue.items,
  userRole: state.userRole,
})

const mapDispatchToProps = dispatch => ({
  retrieveItems: () => { dispatch(retrieveItemsAction()) },
  removeAll: () => { dispatch(new RemoveAllItemsAction().plain()) },
})

export default connect(mapStateToProps, mapDispatchToProps)(InfoBox)
