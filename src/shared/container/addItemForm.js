// @flow

import { connect } from 'react-redux'

import AddItemForm from '../component/add-item-form'
import AddItemAction from '../action/AddItemAction'
import {
  SetName,
  SetDescription,
  SetPrice,
  Reset,
} from '../action/UpdateItemFormAction'


const mapStateToProps = ({ itemForm: { name, description, price }, userRole }) => ({
  name,
  description,
  price,
  userRole,
})

const mapDispatchToProps = dispatch => ({
  updateFormData: (value: any, fieldID: string) => {
    switch (fieldID) {
      case AddItemForm.NAME_FIELD_ID:
        dispatch(new SetName((value: string)).plain())
        break

      case AddItemForm.DESCRIPTION_FIELD_ID:
        dispatch(new SetDescription((value: string)).plain())
        break

      case AddItemForm.PRICE_FIELD_ID:
        dispatch(new SetPrice(parseFloat(value)).plain())
        break

      default:
        break
    }
  },
  addItem: (title: string, description: string, price: number) => {
    dispatch(new AddItemAction({
      title, description, price, picURL: '',
    }).plain())
    dispatch(new Reset().plain())
  },
})

export default connect(mapStateToProps, mapDispatchToProps)(AddItemForm)
