// @flow

import { connect } from 'react-redux'

import Catalogue from '../component/catalogue'
import RemoveItemAction from '../action/RemoveItemAction'
// import { retrieveItemsAction } from '../action/RetrieveItemsAction'


const mapStateToProps = state => ({
  items: state.catalogue.items,
  isPending: state.catalogue.isPending,
})

const mapDispatchToProps = dispatch => ({
  retrieveItems: () => { /* dispatch(retrieveItemsAction()) */ },
  removeItem: (id: string) => { dispatch(new RemoveItemAction(id).plain()) },
})

export default connect(mapStateToProps, mapDispatchToProps)(Catalogue)
