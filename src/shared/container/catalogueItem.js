// @flow

import { connect } from 'react-redux'

import CatalogueItem from '../component/catalogue-item'


const mapStateToProps = state => ({
  userRole: state.userRole,
})

export default connect(mapStateToProps)(CatalogueItem)
