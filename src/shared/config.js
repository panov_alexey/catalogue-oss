// @flow

export const WEB_PORT = process.env.PORT || 8000
export const STATIC_PATH = '/static'
export const APP_NAME = 'Simple Catalogue'

export const WDS_PORT = 7000

export const APP_CONTAINER_CLASS = 'js-app'
export const APP_CONTAINER_SELECTOR = `.${APP_CONTAINER_CLASS}`

export const JSS_SSR_CLASS = 'jss-ssr'
export const JSS_SSR_SELECTOR = `.${JSS_SSR_CLASS}`

export const SIMPLE_USER_ROLE = 'SIMPLE_USER'
export const ADMIN_USER_ROLE = 'ADMIN_USER'
export const DEFAULT_USER_ROLE = ADMIN_USER_ROLE
