// @flow

import BaseAction from '../action/BaseAction'
import {
  SetName,
  SetDescription,
  SetPrice,
  Reset,
} from '../action/UpdateItemFormAction'


type State = {
  +name: string,
  +description: string,
  +price: number,
}

const initialState: State = {
  name: '',
  description: '',
  price: 0,
}

const itemFormReducer = (state: State = initialState, action: BaseAction): State => {
  switch (action.type) {
    case SetName.TYPE:
      return { ...state, name: ((action: any): SetName).value }

    case SetDescription.TYPE:
      return { ...state, description: ((action: any): SetDescription).value }

    case SetPrice.TYPE:
      return { ...state, price: ((action: any): SetPrice).value }

    case Reset.TYPE:
      return initialState

    default:
      return state
  }
}

export default itemFormReducer
