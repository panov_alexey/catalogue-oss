// @flow

import BaseAction from '../action/BaseAction'
import SetUserRoleAction from '../action/SetUserRoleAction'
import { DEFAULT_USER_ROLE } from '../config'


export type State = string

const initialState: State = DEFAULT_USER_ROLE

const userRoleReducer = (state: State = initialState, action: BaseAction): State => {
  switch (action.type) {
    case SetUserRoleAction.TYPE:
      const userRoleAction: SetUserRoleAction = ((action: any): SetUserRoleAction);
      return userRoleAction.value

    default:
      return state
  }
}

export default userRoleReducer
