// @flow

// TODO: this is weird. Needs a lot of refactoring to use Flow properly :(
// https://blog.callstack.io/type-checking-react-and-redux-thunk-with-flow-part-2-206ce5f6e705

import { combineReducers } from 'redux'

import itemsReducer from './items'
import isPendingReducer from './isPending'
import itemFormReducer from './itemForm'
import userRoleReducer from './userRole'


export default combineReducers({
  catalogue: combineReducers({
    items: itemsReducer,
    isPending: isPendingReducer,
  }),
  itemForm: itemFormReducer,
  userRole: userRoleReducer,
})
