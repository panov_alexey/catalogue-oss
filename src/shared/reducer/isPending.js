// @flow

import BaseAction from '../action/BaseAction'
import SetPendingAction from '../action/SetPendingAction'


export type State = boolean

const initialState: State = false;

const setPendingReducer = (state: State = initialState, action: BaseAction): State => {
  switch (action.type) {
    case SetPendingAction.TYPE:
      const setPendingAction: SetPendingAction = ((action: any): SetPendingAction);
      return setPendingAction.value

    default:
      return state
  }
}

export default setPendingReducer
