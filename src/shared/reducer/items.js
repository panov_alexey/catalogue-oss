// @flow

import BaseAction from '../action/BaseAction'
import AddItemAction from '../action/AddItemAction'
import RemoveItemAction from '../action/RemoveItemAction'
import RemoveAllItemsAction from '../action/RemoveAllItemsAction'
import {
  RetrieveItemsActionRequest,
  RetrieveItemsActionSuccess,
  RetrieveItemsActionFail,
} from '../action/RetrieveItemsAction'


export type Item = {
  +id: string,
  +title: string,
  +description: string,
  +picURL: string,
  +price: number,
}
export type State = Array<Item>

const initialState: State = [];

const itemsReducer = (state: State = initialState, action: BaseAction): State => {
  switch (action.type) {
    case AddItemAction.TYPE:
      const addItemAction: AddItemAction = ((action: any): AddItemAction);
      return [...state, addItemAction.item]

    case RemoveItemAction.TYPE:
      const removeItemAction: RemoveItemAction = ((action: any): RemoveItemAction);
      return state.filter(item => item.id !== removeItemAction.id)

    case RemoveAllItemsAction.TYPE:
      return initialState

    case RetrieveItemsActionRequest.TYPE:
      return state

    case RetrieveItemsActionSuccess.TYPE:
      const actualAction: RetrieveItemsActionSuccess = ((action: any): RetrieveItemsActionSuccess);
      return [...actualAction.items]

    case RetrieveItemsActionFail.TYPE:
      return state

    default:
      return state
  }
}

export default itemsReducer
