// @flow
/* eslint-disable react/prefer-stateless-function */

import React from 'react'
import Helmet from 'react-helmet'
import { Switch } from 'react-router'
import { Route } from 'react-router-dom'

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

import HomePage from './component/home-page'
import AddItemPage from './component/add-item-page'
import NotFoundPage from './component/not-found-page'
import { APP_NAME } from './config'
import {
  HOME_PAGE_ROUTE,
  ADD_ITEM_PAGE_ROUTE,
} from './routes'


type Props = {
  muiTheme?: any
}

class App extends React.Component<Props> {
  static defaultProps: Props = {
    muiTheme: null,
  }

  render() {
    const { muiTheme } = this.props

    return (
      <MuiThemeProvider muiTheme={muiTheme}>
        <div>
          <Helmet titleTemplate={`%s | ${APP_NAME}`} defaultTitle={APP_NAME} />
          <Switch>
            <Route exact path={HOME_PAGE_ROUTE} render={() => <HomePage />} />
            <Route path={ADD_ITEM_PAGE_ROUTE} render={() => <AddItemPage />} />
            <Route component={NotFoundPage} />
          </Switch>
        </div>
      </MuiThemeProvider>
    )
  }
}

export default App
