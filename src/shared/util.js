// @flow

// eslint-disable-next-line import/prefer-default-export
export const isProd = process.env.NODE_ENV === 'production'

/**
 *
 */
export const formatNumber = (value: number, fixedPart: number = 2): string => {
  const valueStr = value.toFixed(fixedPart)

  return [].reduceRight.call(valueStr, (prev, current, index) => {
    const i = valueStr.length - (index + 1)
    const d = i > fixedPart + 1 && i < valueStr.length && i % 3 === 0 ? ' ' : ''

    return `${current}${d}${prev}`
  })
}
