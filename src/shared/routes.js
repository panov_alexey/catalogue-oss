// @flow

export const HOME_PAGE_ROUTE = '/'
export const ADD_ITEM_PAGE_ROUTE = '/add-item'
export const NOT_FOUND_DEMO_PAGE_ROUTE = '/404'

export const CATALOGUE_ITEMS_ROUTE = '/ajax/catalogue/items'
