// @flow

import BaseAction from './BaseAction'
import type { Item } from '../reducer/items'

export type ItemData = {
  title: string,
  description: string,
  picURL: string,
  price: number,
}

export default class AddItemAction extends BaseAction {
  static TYPE: string = 'ADD_ITEM_ACTION';
  static ITEMS_COUNTER: number = 0;

  +item: Item

  constructor(data: ItemData) {
    super(AddItemAction.TYPE)

    const {
      title, description, picURL, price,
    } = data

    this.item = {
      id: `${++AddItemAction.ITEMS_COUNTER}`,
      picURL: picURL || 'no-preview.jpg',
      title,
      description,
      price,
    }
  }
}
