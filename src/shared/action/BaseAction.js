// @flow

export default class BaseAction {
  +type: string

  constructor(type: string) {
    this.type = type
  }

  plain(): any {
    return {
      ...this,
    }
  }
}
