// @flow

import BaseAction from './BaseAction'
import SetPendingAction from './SetPendingAction'
import AddItemAction from './AddItemAction'
import * as ls from '../../client/localStorage'
import { CATALOGUE_ITEMS_ROUTE } from '../routes'
import type { Item } from '../reducer/items'


export class RetrieveItemsActionRequest extends BaseAction {
  static TYPE: string = 'RETRIEVE_ITEMS_ACTION_REQUEST';

  constructor() {
    super(RetrieveItemsActionRequest.TYPE)
  }
}

export class RetrieveItemsActionSuccess extends BaseAction {
  static TYPE: string = 'RETRIEVE_ITEMS_ACTION_SUCCESS';

  items: Array<Item>

  constructor(items: Array<Item>) {
    super(RetrieveItemsActionSuccess.TYPE)

    this.items = items
  }
}

export class RetrieveItemsActionFail extends BaseAction {
  static TYPE: string = 'RETRIEVE_ITEMS_ACTION_FAIL';

  constructor() {
    super(RetrieveItemsActionFail.TYPE)
  }
}

export const parseCatalogue = (catalogue: Array<any>): Array<Item> => catalogue.map(itemData => (
  new AddItemAction(itemData).item
))

export const retrieveItemsAction = () => (dispatch: Function) => {
  const lsCatalogue = ls.getCatalogue()

  if (!lsCatalogue) {
    dispatch(new SetPendingAction(true).plain())
    return fetch(CATALOGUE_ITEMS_ROUTE, { method: 'GET' })
      .then((res) => {
        if (!res.ok) throw Error(res.statusText)
        return res.json()
      })
      .then((data) => {
        if (!data.catalogue) throw Error('No items received')

        setTimeout(() => {
          const items = parseCatalogue(data.catalogue)
          dispatch(new RetrieveItemsActionSuccess(items).plain())
          dispatch(new SetPendingAction(false).plain())
        }, 1500)
      })
      .catch(() => {
        dispatch(new RetrieveItemsActionFail().plain())
        dispatch(new SetPendingAction(false).plain())
      })
  }

  // flow-disable-next-line
  return Promise.resolve()
    .then(() => {
      const items = parseCatalogue(lsCatalogue)
      dispatch(new RetrieveItemsActionSuccess(items).plain())
    })
}
