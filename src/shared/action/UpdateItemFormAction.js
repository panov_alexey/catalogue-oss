// @flow
/* eslint-disable import/prefer-default-export */

import BaseAction from './BaseAction'


export class SetName extends BaseAction {
  static TYPE: string = 'SET_NAME';

  +value: string

  constructor(value: string) {
    super(SetName.TYPE)

    this.value = value
  }
}

export class SetDescription extends BaseAction {
  static TYPE: string = 'SET_DESCRIPTION';

  +value: string

  constructor(value: string) {
    super(SetDescription.TYPE)

    this.value = value
  }
}

export class SetPrice extends BaseAction {
  static TYPE: string = 'SET_PRICE';

  +value: number

  constructor(value: number) {
    super(SetPrice.TYPE)

    this.value = value
  }
}

export class Reset extends BaseAction {
  static TYPE: string = 'RESET';

  constructor() {
    super(Reset.TYPE)
  }
}
