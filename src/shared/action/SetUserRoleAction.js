// @flow

import BaseAction from './BaseAction'


export default class SetUserRoleAction extends BaseAction {
  static TYPE: string = 'SET_USER_ROLE_ACTION';

  +value: string

  constructor(value: string) {
    super(SetUserRoleAction.TYPE)

    this.value = value
  }
}
