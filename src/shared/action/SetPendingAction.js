// @flow

import BaseAction from './BaseAction'


export default class SetPendingAction extends BaseAction {
  static TYPE: string = 'SET_PENDING_ACTION';

  +value: boolean

  constructor(value: boolean) {
    super(SetPendingAction.TYPE)

    this.value = value
  }
}
