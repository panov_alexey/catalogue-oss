// @flow

import BaseAction from './BaseAction'


export default class RemoveAllItemsAction extends BaseAction {
  static TYPE: string = 'REMOVE_ALL_ITEMS_ACTION';

  constructor() {
    super(RemoveAllItemsAction.TYPE)
  }
}
