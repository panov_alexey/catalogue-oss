// @flow

import BaseAction from './BaseAction'


export default class RemoveItemAction extends BaseAction {
  static TYPE: string = 'REMOVE_ITEM_ACTION';

  +id: string

  constructor(id: string) {
    super(RemoveItemAction.TYPE)

    this.id = id
  }
}
